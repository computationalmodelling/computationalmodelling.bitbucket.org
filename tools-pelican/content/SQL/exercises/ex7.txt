SELECT Characters_Table.Character, Characters_Table.ChapterName, Books_Table.ChapterNumber
FROM Characters_Table
JOIN Books_Table ON Characters_Table.ChapterName = Books_Table.Name
WHERE Characters_Table.Forename LIKE "S%a"
      AND Characters_Table.book = "A Game of Thrones"
      AND Books_Table.book = "A Game of Thrones"
