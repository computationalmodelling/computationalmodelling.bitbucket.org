""" Task 4: Convert Colorspaces from BGR to GRAY"""
import cv2 as cv

"""Read an image named: 'logo.png' """
image = cv.imread('logo.png',1)

"""Convert Colorspaces using cv.cvtColor()"""
image_Gray =cv.cvtColor(image, cv.COLOR_BGR2GRAY)

"""Show image"""
cv.imshow('BGR',image)
cv.imshow('Gray',image_Gray)
cv.waitKey(0)
cv.destroyAllWindows()

