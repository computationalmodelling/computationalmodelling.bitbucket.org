title: yt Project Tutorial
authors: Daniel Harding, Matthew House, Richard Hall
date: 2018-05-14
tags: training, yt, python, visualisation
slug: demo-post

Welcome to the yt Project Tutorial.

yt is an open source package for Python that is used to display volumetric data. 



What is yt?
-----------
yt is an open source package for python that is used for
 analysing and visualising volumetric data. 
In essence, volumetric data is data that comes in three dimensions. 
yt has been applied in various fields, from astrophyics to oceanography.


Why use yt?
-----------
yt is open source, straightforward to use and is a package for Python, 
a free and widely distributed and used language.

What can yt do?
---------------
1D, 2D, and 3D data visualisation.

Volume rendering of 3D plots

Make films of the graphs produced 

Integrated Parallel programming support with MPI or openMP

What this workshop covers
--------------------------

Slice plots and how to annotate graphs

Phase plots

Particle Plots

Volumetric Rendering



Set up
=======

In terminal, type the following to launch Spyder

	#!bash
	export PATH=/home/feeg6003/anaconda3/bin:$PATH
	spyder

Open up a new python program. To use yt and load data, use the following commands

	#!python
	import yt
	
	ds = yt.load(filename)

Where filename is the filepath your data is saved to.
 
If using the Virtual Machine associated with this tutorial, the filepath used is:

"home/feeg6003/Desktop/yt/IsolatedGalaxy/galaxy0030/galaxy0030"


Slice Plots
===========

Slice plots are used to look at a 'slice' of the volumetric data long a specifed plane.
This allows us to track a variable through the domain and look at a 'slice' of 2D data along one dimension. An example of this would be to track the vortex break down of the vortices generated from the endplate of a formula one car.

To initiate a slice plot:

	#!python
	
	plot = yt.SlicePlot(dataset, sliceplane, field, width=(n, unit))
	
	#data set is the loaded data, sliceplane is the axis you wish to slice
	# ('x', 'y', or 'z'), 
	#field is a parameter assigned to the sliceplane, 
	#and width takes in a float, n, 
	#which decides to area of the data you see around the x axis. 
	#Unit is the units given to this measurement


Annotation
==========

There are various ways to annotate plots in yt to make the data clearer to understand.

Contours
--------

One such was is to add contours, which can be done like this:
	
	#!python
	
	plot.annotate_contour(field, ncont = n, clim = (m, k), label = True, \
	plot_args={'linewidths':n, 'color':black, 'linestyles':'dashed')	

Above, n is a random integer, and m and k are floats representitive of the plot data.

Colour Map
--------

We can also changed the colour map of the image produced with:

	#!python
	plot.set_cmap(field = 'density', cmap = 'hot')

Where field is interchangable with other parameters in the data, and cmap has various different variables, such as hot, dusk, algae and kelp.

Velocity Vectors
----------------

Finally, we can add velocity vectors to our plot, by using the following command:

	#!python
	plot.annotate.velocity(factor = n)

Where n is a random integer.


Spheres
=======

It is beneficial to focus on an area of data rather than looking at the whole data set.
We can selec t a sphere of data within the domain by using the command@

	#!python

	Sphere = ds.sphere('c', (r, unit))

Where 'c' means that the sphere will centre on the centre of the data, r is a float number
representitive of radius, and unit is the units of r, which depends on the data.

Phase Plots
===========

Phase plots can be used to plot the distrubution of points either taking the average or 
accumulation of a bin of data.

Default behaviours on yt is to average using the cell mass as a weighting, however this value
can be changed using a variable called weight_field

To do a Phase plot, use the following function:

	#!python 

	plot = yt.PhasePlot(dataset, field1, field2, cell_mass, weight_field = None)

Where dataset is your data, field1 is a field of the recorded data, field2 is another, seperate
field. cell_mass is derived from the data and weight_field controls the behaviour of averaging. By default, 
weight_field is equivelant to cell_mass.

Setting Units
--------------

You may also want to set the units of the cell mass, as by default it may not be suitable for
the data you are using. (For example, plotting star mass in grams)

	#!python
	plot.set_units('cell_mass', 'unit') 

Where cell_mass is the parameter you wish to change, and unit is the unit you wish to give that
parameter. yt will automatically adjust the graph to suit this. 

Particle Plots
===============
Useful for viewing individual data points in a dataset.

	#!python
	plot = yt.ParticlePlot(dataset, field1, field2, field3, width=(n, m))

dataset is the loaded data, field1 is the x-axis parameter, field2 the y-axis parameter
field3 is the z-axis parameter. width here takes two floats, n and m as zoom factors.

Setting Axis Unit
-----------------
Use this command to give a unit to two axis that have the same unit

	#!python
	plot.set_axes_unit('unit')

For our particle plot, the unit is 'kpc' (but can be 'miles', 'cm', 'au')


Volume Rendering
=================
yt can create 3D visualisations using a process known as volume rendering.

This code evaluates the radiative transfer equations through the volume with user-defined
transfer functions for each ray. Thus it can accommodate both opaque and transparent structures
appropriately. 

Currently all rendering capabilities are implemented in software, and therefore require no 
specialist hardware. 

Constructing a 3D visualisation is a process of describing the "scene" that will be rendered.
This means marking out the location of the viewing point, the method the system is viewed by
and what components will be rendered.

The following is an all in one command as it does a lot of the background setup ready to
render the field that it has been given.

	#!python
	im, sc = yt.volume_render, field, fname = 'Name')

For our use case, dataset is the loaded data, field is a paramter of the data gathered,
and fname is the name given to the file. If this is left out, it will still produce a file.

Setting Camera Width or Zoom
-----------------------------


	#!python
	sc.camera.width = (ds.domain_Width * n)

n is a float centred on 1. To zoom in, us a number less than one, and to zoom out, use a number
greater than one.



And that concludes an introduction to yt.
