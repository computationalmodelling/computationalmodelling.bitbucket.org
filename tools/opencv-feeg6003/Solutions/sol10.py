"""Task 10: Plot histogram of channel 'r'(index=2) with histSize = [256](full scale)"""
import cv2 as cv
from matplotlib import pyplot as plt

"""Read an image"""
img = cv.imread('southampton.jpg')

"""Calculate histogram"""
hist = cv.calcHist(img,[2],None,[256],[0,256]) 
plt.plot(hist, color = 'r')
plt.xlim([0,256])
plt.show()

