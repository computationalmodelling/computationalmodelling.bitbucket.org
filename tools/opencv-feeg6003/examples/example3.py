import cv2
import numpy as nump

# Load an image in color
colorimg = cv2.imread('atlantis.jpg',1)

# From the previous image colorimg
print colorimg.shape

# From the previous image 'colorimg'
pixel = colorimg[50,80]
print pixel

# The split function of an image returns the three different channels separated
b,g,r = cv2.split(colorimg)

# To get again the genuine matrix the merge funtion can be used
colorimg_merged = cv2.merge((b,g,r))

# Modifying all the channels in one step
colorimg[100,100] = [255,255,255]
print colorimg[100,100]
